# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [1.4.0] - 2019-03-19

### Modified
- Added proxy_url and proxy_port params to constructor

## [1.3.0] - 2019-02-13

### Modified
- Backward incompatible change: make check_address return a named tuple rather than a dict


## [1.2.1] - 2019-02-12

### Modified
- Catch `requests` errors and serve them as `SwisspostNoService` exceptions
- Add timeouts wherever necessary.


## [1.2.0] - 2019-02-07

### Modified
- project structure to be installable
- Backported Sergio's modifications from finservice API

## [1.1.0] - 2018-10-30

### Added
- Support for special characters in values (è,é,ü, ...)
- utf-8 encoding in soap xml's body

## [1.0.0] - 2018-10-30
Initial commit, first working version


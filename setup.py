# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

from address_check import __version__

setup(
    name='address_check',
    version=__version__,
    packages=find_packages(exclude=['tests*']),
    license='License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
    description='Python client to the Swiss Post Address Checking service',
    long_description=open('README.md').read(),
    install_requires=[
        'requests', 'xmltodict'
    ],
    url='https://github.com/twistlab/address-check',
    author='Twist Lab',
    author_email='dev@twistlab.ch',
    scripts=['bin/address-check-shell.py']
)
# -*- coding: utf-8 -*-
from collections import namedtuple
from datetime import datetime
import logging
from xml.sax.saxutils import escape

import requests
import xmltodict


logger = logging.getLogger(__name__)

# from requests doc: timeout is not a time limit on the entire response download; rather, an exception is raised if the
# server has not issued a response for timeout seconds (more precisely, if no bytes have been received on the
# underlying socket for timeout seconds). If no timeout is specified explicitly, requests do not time out.
DEFAULT_TIMEOUT = 4.0
DEFAULT_SERVICE_URL = 'https://webservices.post.ch/IN_ADRCHECKERxV4xEXTERNE/V4-02-00'


class SwisspostAddressChecker:
    """
    Main class to query the Swisspost API
    """
    def __init__(self, username, password, timeout=DEFAULT_TIMEOUT, service_url=DEFAULT_SERVICE_URL,
                 proxy_url=None, proxy_port=None):
        """
        :param username: La Poste Technical User (TU): TU_XXXXXXX_XXXX
        :param password: La Poste Technical User's password
        :param timeout: timeout of connections
        :param service_url: La Poste webservice url
        :param proxy_url: Use this as a proxy
        :param proxy_port: proxy port if proxy_url is set
        """
        self.username = username
        self.password = password
        self.timeout = timeout
        self.url = service_url
        self.headers = {'content-type': 'text/xml'}
        self.current_protocol = []
        self.query_body = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v4="http://post.ch/AdressCheckerExtern/V4-02-00"><soapenv:Header/><soapenv:Body><v4:AdressCheckerRequest><Params><MaxRows></MaxRows><CallUser>{}</CallUser><SearchLanguage>{}</SearchLanguage><SearchType>{}</SearchType></Params>{}</v4:AdressCheckerRequest></soapenv:Body></soapenv:Envelope>'
        self.proxies = None
        if proxy_url and proxy_port:
            self.proxies = {
                'http': 'https://{}:{}'.format(proxy_url, proxy_port),
                'https': 'https://{}:{}'.format(proxy_url, proxy_port),
            }

    def check_address(self, search_language, names, street, house_number, zip_code, town):
        """
        Check an address validity

        Keyword arguments:
        search_language -- 1: German, 2: Français, 3: Italian
        names -- Names to check for (i.e. Felix Model)
        street -- Street address (i.e. Viktoriastrasse)
        house_number -- Street house number (i.e. 21)
        zip_code -- Zip code (i.e. 3013)
        town -- Town name (i.e. Berne)
        """
        request_params = '<Names>{0}</Names><Street>{1}</Street><HouseNbr>{2}</HouseNbr><Onrp>0</Onrp>' \
                         '<Zip>{3}</Zip><Town>{4}</Town><HouseKey></HouseKey><PboxAddress></PboxAddress>' \
                         '<PboxNbr></PboxNbr>'

        self._protocol("TITLE", "BEGINNING OF ADDRESS CHECKING PROTOCOL", False)
        self._protocol("INFO", "Protocoling started", True)
        self._protocol("TITLE", "Inputs:", False)
        self._protocol("INFO", "SearchLanguage: " + search_language, False)
        self._protocol("INFO", "SearchType: 1", False)
        self._protocol("INFO", "Names: " + names, False)
        self._protocol("INFO", "Street: " + street, False)
        self._protocol("INFO", "House Number: " + house_number, False)
        self._protocol("INFO", "Zip Code: " + zip_code, False)
        self._protocol("INFO", "Town: " + town, False)

        request_params = request_params.format(self._enc(names), self._enc(street), self._enc(house_number),
                                               self._enc(zip_code), self._enc(town))

        body = self.query_body.format(self._enc(self.username), self._enc(search_language), 1, request_params)
        logger.debug(body)

        self._protocol("TITLE", "Raw query data (xml)", False)
        self._protocol("INFO", str(body), False)
        self._protocol("INFO", "Calling: " + str(self.url), True)
        try:
            response = requests.post(self.url, data=body,
                                     headers=self.headers, auth=(self.username, self.password),
                                     timeout=self.timeout, proxies=self.proxies)
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError) as e:
            logger.error('Connection to swiss post service was not made: {}'.format(e))
            raise SwisspostNoService() from e
        if response.content is None:
            logger.error('Empty response from swiss post service')
            raise SwisspostTechnicalError()

        logger.debug(response.content)
        self._protocol("INFO", "Response received", True)
        self._protocol("TITLE", "Raw reponse data (xml)", False)
        self._protocol("INFO", response.content, False)
        self._protocol("INFO", "Protocoling ended", True)
        self._protocol("TITLE", "END OF PROTOCOL", False)
        query_protocol = self.current_protocol
        self.current_protocol = []
        swisspost_response = SwisspostAddressCheckerResponse(response, query_protocol)

        # remove username information for security reasons
        body = body.replace(self._enc(self.username), "")
        SwisspostAddressCheckResponse = namedtuple('SwisspostAddressCheckResponse',
                                                   ('request', 'response', 'protocol', 'result'))
        return SwisspostAddressCheckResponse(
            request=body,
            response=response.content,
            protocol=swisspost_response.printable_protocol,
            result=swisspost_response.content
        )

    def autocomplete_zip(self, zip_code, town):
        request_params = '<Names></Names><Street></Street><HouseNbr></HouseNbr><Onrp>0</Onrp>' \
                         '<Zip>{0}</Zip><Town>{1}</Town><HouseKey></HouseKey><PboxAddress></PboxAddress>' \
                         '<PboxNbr></PboxNbr>'

        self._protocol("TITLE", "BEGINING OF ADDRESS CHECKING PROTOCOL", False)
        self._protocol("INFO", "Protocoling started", True)
        self._protocol("TITLE", "Inputs:", False)
        self._protocol("INFO", "Zip Code: " + zip_code, False)
        self._protocol("INFO", "Town: " + town, False)

        request_params = request_params.format(self._enc(zip_code), self._enc(town))
        body = self.query_body.format(self._enc(self.username), self._enc('2'), 10, request_params)

        self._protocol("TITLE", "Raw query data (xml)", False)
        self._protocol("INFO", str(body), False)
        self._protocol("INFO", "Calling: " + str(self.url), True)
        try:
            response = requests.post(self.url, data=body, headers=self.headers, auth=(self.username, self.password),
                                     timeout=self.timeout, proxies=self.proxies)
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError) as e:
            logger.error('Connection to swiss post service was not made: {}'.format(e))
            raise SwisspostNoService() from e
        if response.content is None:
            logger.error('Empty response from swiss post service')
            raise SwisspostTechnicalError()
        self._protocol("INFO", "Response received", True)
        self._protocol("TITLE", "Raw reponse data (xml)", False)
        self._protocol("INFO", response.content, False)
        self._protocol("INFO", "Protocoling ended", True)
        self._protocol("TITLE", "END OF PROTOCOL", False)

        query_protocol = self.current_protocol
        self.current_protocol = []
        return SwisspostAddressCheckerResponse(response, query_protocol)

    @staticmethod
    def _enc(to_encode):
        """
        Internal XML encoding method, to avoid problems with accents and various other charaters (i.e. %&*ç`)

        Keyword arguments:
        to_encode -- The string to encode
        """
        return escape(to_encode).encode('ascii', 'xmlcharrefreplace').decode("utf-8")

    def _protocol(self, level, message, timestamp):
        """
        Internal protocoling method

        Keyword arguments:
        level -- The message level (INFO / TITLE)
        message -- The message to protocol
        timestamp -- True/False: record timestamp or not
        """
        if timestamp:
            self.current_protocol.append((datetime.now(), level, message))
        else:
            self.current_protocol.append((None, level, message))


class SwisspostAddressCheckerResponse:

    def __init__(self, response, current_protocol):
        """
        Constructor

        Keyword arguments:
        response -- the raw response object (from requests)
        current_protocol -- the full protocol of operations
        """
        self.response = response
        self.current_protocol = current_protocol
        self.content = []
        self._parse_response(self.response)

    def __str__(self):
        """
        Redefining string representation of class to display response
        """
        response = ""
        for row in self.content:
            for key, value in row.items():
                response += str(key) + " [" + str(value) + "]\n"
        return response

    def _parse_response(self, response):
        """
        Parse the reponse object and populate class variables
        """
        parsed_response = xmltodict.parse(response.content)
        self._check_for_error(parsed_response)
        data = parsed_response['soap:Envelope']['soap:Body']['ns3:AdressCheckerResponse']['Rows']
        if isinstance(data, list):
            for row in data:
                self.content.append(self._parse_row(row))

        else:
            self.content = self._parse_row(data)

    @staticmethod
    def _parse_row(row):
        element = {}
        # Getting strings and integers
        for item in ('MatchUniqueness', 'MatchType', 'MatchHistoric', 'GuaranteedDelivery', 'PboxZip', 'PboxTown',
                     'HouseKey', 'Street', 'StreetFormatted', 'HouseNbr', 'DeliveryPointHouseKey',
                     'AlternativeStreet', 'StreetOfficial', 'Onrp', 'Zip', 'Town18', 'Town27', 'ZipLang',
                     'ZipType', 'AdditionalOnrp', 'AdditionalZip', 'AdditionalTown18', 'AdditionalTown27',
                     'AdditionalZipType', 'AdditionalZipLang'):
            if item in row:
                try:
                    element[item] = int(row[item])
                except ValueError:
                    element[item] = row[item]

        # Getting booleans
        for item in ('HasPbox', 'DeliveryPoint', 'AddressOfficial', 'StreetValid', 'HouseNbrValid', 'ZipValid',
                     'TownValid', 'TownOfficial'):
            if item in row:
                element[item] = row[item] == '1'

        return element

    @staticmethod
    def _check_for_error(parsed_response):
        """
        Check for error based on response code and throw the proper exception
        """
        if 'Fault' in parsed_response['soap:Envelope']['soap:Body']['ns3:AdressCheckerResponse']:
            code = parsed_response['soap:Envelope']['soap:Body']['ns3:AdressCheckerResponse']['Fault']['FaultNr']
            msg = parsed_response['soap:Envelope']['soap:Body']['ns3:AdressCheckerResponse']['Fault']['FaultMsg']
            code_to_exception = {
                '0': SwisspostInvalidSearch,
                '1': SwisspostNoService,
                '2': SwisspostNoSearchResults,
                '3': SwisspostTooManyResults,
                '4': SwisspostIncompleteSearchCriteria,
                '5': SwisspostInvalidSearchParameters,
                '7': SwisspostSearchTimeout,
                '8': SwisspostClusterError,
                '9': SwisspostTechnicalError,

            }
            raise code_to_exception[code](msg)

    @property
    def printable_protocol(self):
        """
        Return the protocol as a print friendly string
        """
        protocol_to_return = ""
        for protocolentry in self.current_protocol:
            date_and_time = "" if not protocolentry[0] else "[" + str(protocolentry[0]) + "] "
            if protocolentry[1] == "INFO":
                protocol_to_return += date_and_time + str(protocolentry[2]) + "\n"
            else:
                protocol_to_return += "****************************************************\n"
                protocol_to_return += date_and_time + str(protocolentry[2]) + "\n"
                protocol_to_return += "****************************************************\n"
        return protocol_to_return


#################################################
# Swisspost Exceptions, please refer to README.md
#################################################


class SwissPostException(Exception):
    pass


class SwisspostInvalidSearch(SwissPostException):
    code = '0'


class SwisspostNoService(SwissPostException):
    code = '1'


class SwisspostNoSearchResults(SwissPostException):
    code = '2'


class SwisspostTooManyResults(SwissPostException):
    code = '3'


class SwisspostIncompleteSearchCriteria(SwissPostException):
    code = '4'


class SwisspostInvalidSearchParameters(SwissPostException):
    code = '5'


class SwisspostSearchTimeout(SwissPostException):
    code = '7'


class SwisspostClusterError(SwissPostException):
    code = '8'


class SwisspostTechnicalError(SwissPostException):
    code = '9'


class SwisspostUserError(SwissPostException):
    code = 'env:Client'

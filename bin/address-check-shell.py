#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from address_check.swisspost import SwisspostAddressChecker


user_input = None
search_language = None

print("----------------------------------------------------")
print("| TWISTLAB - Swiss Post Address Checker            |")
print("----------------------------------------------------")

# SECURITY DETAILS #################################################
print("Technical User ID (TU) [i.e. TU_XXXXXXX_XXXX]:")
username = input()  # <- Technical user ID from La Poste
print("Technical User ID (TU) password [i.e. FV5cSd23]:")
password = input()  # <- Technical user password
####################################################################

addresschecker = SwisspostAddressChecker(username, password)

while user_input != "q" and user_input != 'Q':
    response = None
    search_language = None
    print("--- MENU ---")
    print("[1] Check an address")
    print("[q] Quit")
    user_input = input() 

    if user_input == '1' or user_input == "":
        while search_language != "1" and search_language != "2" and search_language != "3":
            print("--- Search Language ---")
            print("[1] German")
            print("[2] Français")
            print("[3] Italiano")
            search_language = input()
        print("Names [i.e: 'Felix Model']: ")
        names = input()
        print("Street [i.e: 'Viktoriastrasse']: ")
        street = input()
        print("House number [i.e: '21']: ")
        house_number = input()
        print("Zip [i.e: '3013']: ")
        zip_code = input()
        print("Town [i.e: 'Berne']: ")
        town = input()
        try:
            response = addresschecker.check_address(search_language, names, street, house_number, zip_code, town)
            print("----------------------------------------------------")
            print("Response:")
            print(response.get('result'))
            print("----------------------------------------------------")
            print("Show protocol? [Y/n]")
            show_protocol = input()
            if show_protocol == 'Y' or show_protocol == 'y' or not show_protocol:
                print(response.get('protocol'))

        except Exception as e:
            print("----------------------------------------------------")
            print("An error occured:")
            print(" " + str(e))
            print("----------------------------------------------------")

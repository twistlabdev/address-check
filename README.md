# Twist Lab - Swiss Post Address Checker

This project provides a Python client to the [Swiss Post Address Checking service](https://www.post.ch/en/business/a-z-of-subjects/maintaining-addresses-and-using-geodata/address-verification).


## Dependencies


### Python version

This project uses *Python3*


### Technical User (TU) / Password

You need to have an account on the [Swiss Post Address Checking service](https://www.post.ch/en/business/a-z-of-subjects/maintaining-addresses-and-using-geodata/address-verification) with a valid *Technical User* and *Password* to use this tool


### Python libraries 

Use PIP to install requirements listed in `requirements.txt`:

`pip install -r requirements.txt`


## How to use

The tool can be used using command line with:

`python3 address-check-shell.py`

Otherwise, import **swisspost_address_checker.py** directly in your project and call `checkAddress()` method

## Exceptions

The errors described in the Swiss Post pdf documentation (see [docs/BH_Controle_d_adresses_gratuit_V4.2_FR.PDF](docs/BH_Controle_d_adresses_gratuit_V4.2_FR.PDF)) have been mapped to the following custom **Exceptions** that can be raised by this tool when calling `checkAddress()` method:

### SwisspostInvalidSearch

**Error code 0** Search type is invalid (should not happen with the current version, enforced to type 1)

### SwisspostNoService

**Error code 1** No access service

### SwisspostNoSearchResults

**Error code 2** No search results

### SwisspostTooManyResults

**Error code 3** Too many search results (should not happen with the current version, enforced return only one result)

### SwisspostIncompleteSearchCriteria

**Error code 4** Incomplete search criteria

### SwisspostInvalidSearchParameters

**Error code 5** Invalid search parameters (should not happen with the current version, enforced search parameters)

### SwisspostSearchTimeout

**Error code 7** Timeout on search due to a technical issue. One should try again.

### SwisspostClusterError

**Error code 8** Search cluster error, i.e. search index not available due to a technical issue.

### SwisspostTechnicalError

**Error code 9** Other technical issue

### SwisspostUserError

**Error code env:Client** Username and password not registered or blocked.


---
(c) Twist Lab Sàrl | www.twistlab.ch
